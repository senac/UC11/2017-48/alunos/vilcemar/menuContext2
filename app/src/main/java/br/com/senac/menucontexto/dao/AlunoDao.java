package br.com.senac.menucontexto.dao;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import br.com.senac.menucontexto.Aluno;


public class AlunoDao extends SQLiteOpenHelper{

    private static final String DATABASE = "SQLite";
    private static final int VERSÃO = 1;

    public AlunoDao(Context context) {
        super(context, DATABASE, null, VERSÃO);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String dd1 = "CREATE TABLE Alunos " +
                "( id PRIMARY KEY , " +
                "nome TEXT NOT NULL , " +
                "telefone TEXT  ,  " +
                "email TEXT  , " +
                "site TEXT ) ; " ;
        db.execSQL(dd1);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String dd1 = "DROP TABLE IF EXISTS Aluno";
        db.execSQL(dd1);

    }

    public void salvar(Aluno aluno){
        ContentValues values = new ContentValues();
        values.put("nome", aluno.getNome());
        values.put("telefone", aluno.getTelefone());
        values.put("email", aluno.getEmail());
        values.put("site", aluno.getSite());

        getReadableDatabase().insert( "Alunos",null,values);


    }

    public List <Aluno> GetList(){
        List <Aluno> lista = new ArrayList<>();
        String Colunas[] = {"id", "nome", "telefone" , "email" , "site"};

        Cursor cursor = getWritableDatabase().query("Alunos", Colunas, null,
                null, null, null, null);

        while (cursor.moveToNext()){
            Aluno aluno = new Aluno();
            aluno.setId(cursor.getInt(0));
            aluno.setNome(cursor.getString(1));
            aluno.setTelefone(cursor.getString(2));
            aluno.setEmail(cursor.getString(3));
            aluno.setSite(cursor.getString(4));

            lista.add(aluno);
        }

        return lista;
    };







}
